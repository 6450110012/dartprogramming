

main() {
  printInfo(String name, String gender, [String title = 'sir/ma’am']) {
    print("Hello $title $name your gender is $gender");
  }

  printInfo("John", "Male");
  printInfo("John", "Male", "Mr.");
  printInfo("Kavya", "Female", "Ms.");
}
