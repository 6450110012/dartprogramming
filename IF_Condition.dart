import 'dart:io';

     main() {
  print("Input your character.....");
  String? character = stdin.readLineSync();
  String char =character!.toLowerCase();

  bool charexp = RegExp(r'^[a-zA-Z\s]+$').hasMatch(char);

  if (character.length == 1 && charexp == true) {

    if (charexp == 'a' || char == 'e' || char == 'i' || char == 'o' || char == 'u') {
      print('${character[0]} is vowel.');
    } else {

      print('${character[0]} is consonant.');
    }
  } else if (character.length > 1) {

    print("Please enter 1 character.");
  } else if (charexp == false) {

    print("Please enter English character.");
  }
  else if (character.length == 0) {
    print("Please enter a valid character.");
  }
}
